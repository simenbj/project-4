// Code generated by Prisma (prisma@1.34.10). DO NOT EDIT.
// Please don't change this file manually but run `prisma generate` to update it.
// For more information, please read the docs: https://www.prisma.io/docs/prisma-client/

import { DocumentNode } from "graphql";
import {
  makePrismaClientClass,
  BaseClientOptions,
  Model
} from "prisma-client-lib";
import { typeDefs } from "./prisma-schema";

export type AtLeastOne<T, U = { [K in keyof T]: Pick<T, K> }> = Partial<T> &
  U[keyof U];

export type Maybe<T> = T | undefined | null;

export interface Exists {
  address: (where?: AddressWhereInput) => Promise<boolean>;
  name: (where?: NameWhereInput) => Promise<boolean>;
  person: (where?: PersonWhereInput) => Promise<boolean>;
}

export interface Node {}

export type FragmentableArray<T> = Promise<Array<T>> & Fragmentable;

export interface Fragmentable {
  $fragment<T>(fragment: string | DocumentNode): Promise<T>;
}

export interface Prisma {
  $exists: Exists;
  $graphql: <T = any>(
    query: string,
    variables?: { [key: string]: any }
  ) => Promise<T>;

  /**
   * Queries
   */

  address: (where: AddressWhereUniqueInput) => AddressNullablePromise;
  addresses: (args?: {
    where?: AddressWhereInput;
    orderBy?: AddressOrderByInput;
    skip?: Int;
    after?: String;
    before?: String;
    first?: Int;
    last?: Int;
  }) => FragmentableArray<Address>;
  addressesConnection: (args?: {
    where?: AddressWhereInput;
    orderBy?: AddressOrderByInput;
    skip?: Int;
    after?: String;
    before?: String;
    first?: Int;
    last?: Int;
  }) => AddressConnectionPromise;
  name: (where: NameWhereUniqueInput) => NameNullablePromise;
  names: (args?: {
    where?: NameWhereInput;
    orderBy?: NameOrderByInput;
    skip?: Int;
    after?: String;
    before?: String;
    first?: Int;
    last?: Int;
  }) => FragmentableArray<Name>;
  namesConnection: (args?: {
    where?: NameWhereInput;
    orderBy?: NameOrderByInput;
    skip?: Int;
    after?: String;
    before?: String;
    first?: Int;
    last?: Int;
  }) => NameConnectionPromise;
  person: (where: PersonWhereUniqueInput) => PersonNullablePromise;
  persons: (args?: {
    where?: PersonWhereInput;
    orderBy?: PersonOrderByInput;
    skip?: Int;
    after?: String;
    before?: String;
    first?: Int;
    last?: Int;
  }) => FragmentableArray<Person>;
  personsConnection: (args?: {
    where?: PersonWhereInput;
    orderBy?: PersonOrderByInput;
    skip?: Int;
    after?: String;
    before?: String;
    first?: Int;
    last?: Int;
  }) => PersonConnectionPromise;
  node: (args: { id: ID_Output }) => Node;

  /**
   * Mutations
   */

  createAddress: (data: AddressCreateInput) => AddressPromise;
  updateAddress: (args: {
    data: AddressUpdateInput;
    where: AddressWhereUniqueInput;
  }) => AddressPromise;
  updateManyAddresses: (args: {
    data: AddressUpdateManyMutationInput;
    where?: AddressWhereInput;
  }) => BatchPayloadPromise;
  upsertAddress: (args: {
    where: AddressWhereUniqueInput;
    create: AddressCreateInput;
    update: AddressUpdateInput;
  }) => AddressPromise;
  deleteAddress: (where: AddressWhereUniqueInput) => AddressPromise;
  deleteManyAddresses: (where?: AddressWhereInput) => BatchPayloadPromise;
  createName: (data: NameCreateInput) => NamePromise;
  updateName: (args: {
    data: NameUpdateInput;
    where: NameWhereUniqueInput;
  }) => NamePromise;
  updateManyNames: (args: {
    data: NameUpdateManyMutationInput;
    where?: NameWhereInput;
  }) => BatchPayloadPromise;
  upsertName: (args: {
    where: NameWhereUniqueInput;
    create: NameCreateInput;
    update: NameUpdateInput;
  }) => NamePromise;
  deleteName: (where: NameWhereUniqueInput) => NamePromise;
  deleteManyNames: (where?: NameWhereInput) => BatchPayloadPromise;
  createPerson: (data: PersonCreateInput) => PersonPromise;
  updatePerson: (args: {
    data: PersonUpdateInput;
    where: PersonWhereUniqueInput;
  }) => PersonPromise;
  updateManyPersons: (args: {
    data: PersonUpdateManyMutationInput;
    where?: PersonWhereInput;
  }) => BatchPayloadPromise;
  upsertPerson: (args: {
    where: PersonWhereUniqueInput;
    create: PersonCreateInput;
    update: PersonUpdateInput;
  }) => PersonPromise;
  deletePerson: (where: PersonWhereUniqueInput) => PersonPromise;
  deleteManyPersons: (where?: PersonWhereInput) => BatchPayloadPromise;

  /**
   * Subscriptions
   */

  $subscribe: Subscription;
}

export interface Subscription {
  address: (
    where?: AddressSubscriptionWhereInput
  ) => AddressSubscriptionPayloadSubscription;
  name: (
    where?: NameSubscriptionWhereInput
  ) => NameSubscriptionPayloadSubscription;
  person: (
    where?: PersonSubscriptionWhereInput
  ) => PersonSubscriptionPayloadSubscription;
}

export interface ClientConstructor<T> {
  new (options?: BaseClientOptions): T;
}

/**
 * Types
 */

export type AddressOrderByInput =
  | "id_ASC"
  | "id_DESC"
  | "street_ASC"
  | "street_DESC"
  | "city_ASC"
  | "city_DESC"
  | "state_ASC"
  | "state_DESC"
  | "zip_ASC"
  | "zip_DESC";

export type NameOrderByInput =
  | "id_ASC"
  | "id_DESC"
  | "first_ASC"
  | "first_DESC"
  | "last_ASC"
  | "last_DESC";

export type PersonOrderByInput =
  | "id_ASC"
  | "id_DESC"
  | "username_ASC"
  | "username_DESC"
  | "password_ASC"
  | "password_DESC"
  | "gender_ASC"
  | "gender_DESC";

export type MutationType = "CREATED" | "UPDATED" | "DELETED";

export interface AddressUpdateInput {
  street?: Maybe<String>;
  city?: Maybe<String>;
  state?: Maybe<String>;
  zip?: Maybe<String>;
}

export type AddressWhereUniqueInput = AtLeastOne<{
  id: Maybe<ID_Input>;
}>;

export interface NameUpdateDataInput {
  first?: Maybe<String>;
  last?: Maybe<String>;
}

export interface NameCreateInput {
  id?: Maybe<ID_Input>;
  first: String;
  last: String;
}

export interface PersonWhereInput {
  id?: Maybe<ID_Input>;
  id_not?: Maybe<ID_Input>;
  id_in?: Maybe<ID_Input[] | ID_Input>;
  id_not_in?: Maybe<ID_Input[] | ID_Input>;
  id_lt?: Maybe<ID_Input>;
  id_lte?: Maybe<ID_Input>;
  id_gt?: Maybe<ID_Input>;
  id_gte?: Maybe<ID_Input>;
  id_contains?: Maybe<ID_Input>;
  id_not_contains?: Maybe<ID_Input>;
  id_starts_with?: Maybe<ID_Input>;
  id_not_starts_with?: Maybe<ID_Input>;
  id_ends_with?: Maybe<ID_Input>;
  id_not_ends_with?: Maybe<ID_Input>;
  username?: Maybe<String>;
  username_not?: Maybe<String>;
  username_in?: Maybe<String[] | String>;
  username_not_in?: Maybe<String[] | String>;
  username_lt?: Maybe<String>;
  username_lte?: Maybe<String>;
  username_gt?: Maybe<String>;
  username_gte?: Maybe<String>;
  username_contains?: Maybe<String>;
  username_not_contains?: Maybe<String>;
  username_starts_with?: Maybe<String>;
  username_not_starts_with?: Maybe<String>;
  username_ends_with?: Maybe<String>;
  username_not_ends_with?: Maybe<String>;
  password?: Maybe<String>;
  password_not?: Maybe<String>;
  password_in?: Maybe<String[] | String>;
  password_not_in?: Maybe<String[] | String>;
  password_lt?: Maybe<String>;
  password_lte?: Maybe<String>;
  password_gt?: Maybe<String>;
  password_gte?: Maybe<String>;
  password_contains?: Maybe<String>;
  password_not_contains?: Maybe<String>;
  password_starts_with?: Maybe<String>;
  password_not_starts_with?: Maybe<String>;
  password_ends_with?: Maybe<String>;
  password_not_ends_with?: Maybe<String>;
  name?: Maybe<NameWhereInput>;
  gender?: Maybe<String>;
  gender_not?: Maybe<String>;
  gender_in?: Maybe<String[] | String>;
  gender_not_in?: Maybe<String[] | String>;
  gender_lt?: Maybe<String>;
  gender_lte?: Maybe<String>;
  gender_gt?: Maybe<String>;
  gender_gte?: Maybe<String>;
  gender_contains?: Maybe<String>;
  gender_not_contains?: Maybe<String>;
  gender_starts_with?: Maybe<String>;
  gender_not_starts_with?: Maybe<String>;
  gender_ends_with?: Maybe<String>;
  gender_not_ends_with?: Maybe<String>;
  address?: Maybe<AddressWhereInput>;
  AND?: Maybe<PersonWhereInput[] | PersonWhereInput>;
  OR?: Maybe<PersonWhereInput[] | PersonWhereInput>;
  NOT?: Maybe<PersonWhereInput[] | PersonWhereInput>;
}

export interface AddressUpdateManyMutationInput {
  street?: Maybe<String>;
  city?: Maybe<String>;
  state?: Maybe<String>;
  zip?: Maybe<String>;
}

export interface PersonSubscriptionWhereInput {
  mutation_in?: Maybe<MutationType[] | MutationType>;
  updatedFields_contains?: Maybe<String>;
  updatedFields_contains_every?: Maybe<String[] | String>;
  updatedFields_contains_some?: Maybe<String[] | String>;
  node?: Maybe<PersonWhereInput>;
  AND?: Maybe<PersonSubscriptionWhereInput[] | PersonSubscriptionWhereInput>;
  OR?: Maybe<PersonSubscriptionWhereInput[] | PersonSubscriptionWhereInput>;
  NOT?: Maybe<PersonSubscriptionWhereInput[] | PersonSubscriptionWhereInput>;
}

export interface AddressSubscriptionWhereInput {
  mutation_in?: Maybe<MutationType[] | MutationType>;
  updatedFields_contains?: Maybe<String>;
  updatedFields_contains_every?: Maybe<String[] | String>;
  updatedFields_contains_some?: Maybe<String[] | String>;
  node?: Maybe<AddressWhereInput>;
  AND?: Maybe<AddressSubscriptionWhereInput[] | AddressSubscriptionWhereInput>;
  OR?: Maybe<AddressSubscriptionWhereInput[] | AddressSubscriptionWhereInput>;
  NOT?: Maybe<AddressSubscriptionWhereInput[] | AddressSubscriptionWhereInput>;
}

export interface NameUpdateOneRequiredInput {
  create?: Maybe<NameCreateInput>;
  update?: Maybe<NameUpdateDataInput>;
  upsert?: Maybe<NameUpsertNestedInput>;
  connect?: Maybe<NameWhereUniqueInput>;
}

export type NameWhereUniqueInput = AtLeastOne<{
  id: Maybe<ID_Input>;
}>;

export interface PersonUpdateInput {
  username?: Maybe<String>;
  password?: Maybe<String>;
  name?: Maybe<NameUpdateOneRequiredInput>;
  gender?: Maybe<String>;
  address?: Maybe<AddressUpdateOneInput>;
}

export interface NameWhereInput {
  id?: Maybe<ID_Input>;
  id_not?: Maybe<ID_Input>;
  id_in?: Maybe<ID_Input[] | ID_Input>;
  id_not_in?: Maybe<ID_Input[] | ID_Input>;
  id_lt?: Maybe<ID_Input>;
  id_lte?: Maybe<ID_Input>;
  id_gt?: Maybe<ID_Input>;
  id_gte?: Maybe<ID_Input>;
  id_contains?: Maybe<ID_Input>;
  id_not_contains?: Maybe<ID_Input>;
  id_starts_with?: Maybe<ID_Input>;
  id_not_starts_with?: Maybe<ID_Input>;
  id_ends_with?: Maybe<ID_Input>;
  id_not_ends_with?: Maybe<ID_Input>;
  first?: Maybe<String>;
  first_not?: Maybe<String>;
  first_in?: Maybe<String[] | String>;
  first_not_in?: Maybe<String[] | String>;
  first_lt?: Maybe<String>;
  first_lte?: Maybe<String>;
  first_gt?: Maybe<String>;
  first_gte?: Maybe<String>;
  first_contains?: Maybe<String>;
  first_not_contains?: Maybe<String>;
  first_starts_with?: Maybe<String>;
  first_not_starts_with?: Maybe<String>;
  first_ends_with?: Maybe<String>;
  first_not_ends_with?: Maybe<String>;
  last?: Maybe<String>;
  last_not?: Maybe<String>;
  last_in?: Maybe<String[] | String>;
  last_not_in?: Maybe<String[] | String>;
  last_lt?: Maybe<String>;
  last_lte?: Maybe<String>;
  last_gt?: Maybe<String>;
  last_gte?: Maybe<String>;
  last_contains?: Maybe<String>;
  last_not_contains?: Maybe<String>;
  last_starts_with?: Maybe<String>;
  last_not_starts_with?: Maybe<String>;
  last_ends_with?: Maybe<String>;
  last_not_ends_with?: Maybe<String>;
  AND?: Maybe<NameWhereInput[] | NameWhereInput>;
  OR?: Maybe<NameWhereInput[] | NameWhereInput>;
  NOT?: Maybe<NameWhereInput[] | NameWhereInput>;
}

export interface AddressCreateOneInput {
  create?: Maybe<AddressCreateInput>;
  connect?: Maybe<AddressWhereUniqueInput>;
}

export interface AddressUpdateDataInput {
  street?: Maybe<String>;
  city?: Maybe<String>;
  state?: Maybe<String>;
  zip?: Maybe<String>;
}

export interface NameUpsertNestedInput {
  update: NameUpdateDataInput;
  create: NameCreateInput;
}

export interface NameSubscriptionWhereInput {
  mutation_in?: Maybe<MutationType[] | MutationType>;
  updatedFields_contains?: Maybe<String>;
  updatedFields_contains_every?: Maybe<String[] | String>;
  updatedFields_contains_some?: Maybe<String[] | String>;
  node?: Maybe<NameWhereInput>;
  AND?: Maybe<NameSubscriptionWhereInput[] | NameSubscriptionWhereInput>;
  OR?: Maybe<NameSubscriptionWhereInput[] | NameSubscriptionWhereInput>;
  NOT?: Maybe<NameSubscriptionWhereInput[] | NameSubscriptionWhereInput>;
}

export interface AddressCreateInput {
  id?: Maybe<ID_Input>;
  street: String;
  city: String;
  state: String;
  zip: String;
}

export interface AddressUpsertNestedInput {
  update: AddressUpdateDataInput;
  create: AddressCreateInput;
}

export interface NameUpdateInput {
  first?: Maybe<String>;
  last?: Maybe<String>;
}

export interface NameUpdateManyMutationInput {
  first?: Maybe<String>;
  last?: Maybe<String>;
}

export interface PersonCreateInput {
  id?: Maybe<ID_Input>;
  username: String;
  password: String;
  name: NameCreateOneInput;
  gender: String;
  address?: Maybe<AddressCreateOneInput>;
}

export interface NameCreateOneInput {
  create?: Maybe<NameCreateInput>;
  connect?: Maybe<NameWhereUniqueInput>;
}

export interface AddressWhereInput {
  id?: Maybe<ID_Input>;
  id_not?: Maybe<ID_Input>;
  id_in?: Maybe<ID_Input[] | ID_Input>;
  id_not_in?: Maybe<ID_Input[] | ID_Input>;
  id_lt?: Maybe<ID_Input>;
  id_lte?: Maybe<ID_Input>;
  id_gt?: Maybe<ID_Input>;
  id_gte?: Maybe<ID_Input>;
  id_contains?: Maybe<ID_Input>;
  id_not_contains?: Maybe<ID_Input>;
  id_starts_with?: Maybe<ID_Input>;
  id_not_starts_with?: Maybe<ID_Input>;
  id_ends_with?: Maybe<ID_Input>;
  id_not_ends_with?: Maybe<ID_Input>;
  street?: Maybe<String>;
  street_not?: Maybe<String>;
  street_in?: Maybe<String[] | String>;
  street_not_in?: Maybe<String[] | String>;
  street_lt?: Maybe<String>;
  street_lte?: Maybe<String>;
  street_gt?: Maybe<String>;
  street_gte?: Maybe<String>;
  street_contains?: Maybe<String>;
  street_not_contains?: Maybe<String>;
  street_starts_with?: Maybe<String>;
  street_not_starts_with?: Maybe<String>;
  street_ends_with?: Maybe<String>;
  street_not_ends_with?: Maybe<String>;
  city?: Maybe<String>;
  city_not?: Maybe<String>;
  city_in?: Maybe<String[] | String>;
  city_not_in?: Maybe<String[] | String>;
  city_lt?: Maybe<String>;
  city_lte?: Maybe<String>;
  city_gt?: Maybe<String>;
  city_gte?: Maybe<String>;
  city_contains?: Maybe<String>;
  city_not_contains?: Maybe<String>;
  city_starts_with?: Maybe<String>;
  city_not_starts_with?: Maybe<String>;
  city_ends_with?: Maybe<String>;
  city_not_ends_with?: Maybe<String>;
  state?: Maybe<String>;
  state_not?: Maybe<String>;
  state_in?: Maybe<String[] | String>;
  state_not_in?: Maybe<String[] | String>;
  state_lt?: Maybe<String>;
  state_lte?: Maybe<String>;
  state_gt?: Maybe<String>;
  state_gte?: Maybe<String>;
  state_contains?: Maybe<String>;
  state_not_contains?: Maybe<String>;
  state_starts_with?: Maybe<String>;
  state_not_starts_with?: Maybe<String>;
  state_ends_with?: Maybe<String>;
  state_not_ends_with?: Maybe<String>;
  zip?: Maybe<String>;
  zip_not?: Maybe<String>;
  zip_in?: Maybe<String[] | String>;
  zip_not_in?: Maybe<String[] | String>;
  zip_lt?: Maybe<String>;
  zip_lte?: Maybe<String>;
  zip_gt?: Maybe<String>;
  zip_gte?: Maybe<String>;
  zip_contains?: Maybe<String>;
  zip_not_contains?: Maybe<String>;
  zip_starts_with?: Maybe<String>;
  zip_not_starts_with?: Maybe<String>;
  zip_ends_with?: Maybe<String>;
  zip_not_ends_with?: Maybe<String>;
  AND?: Maybe<AddressWhereInput[] | AddressWhereInput>;
  OR?: Maybe<AddressWhereInput[] | AddressWhereInput>;
  NOT?: Maybe<AddressWhereInput[] | AddressWhereInput>;
}

export interface PersonUpdateManyMutationInput {
  username?: Maybe<String>;
  password?: Maybe<String>;
  gender?: Maybe<String>;
}

export type PersonWhereUniqueInput = AtLeastOne<{
  id: Maybe<ID_Input>;
  username?: Maybe<String>;
}>;

export interface AddressUpdateOneInput {
  create?: Maybe<AddressCreateInput>;
  update?: Maybe<AddressUpdateDataInput>;
  upsert?: Maybe<AddressUpsertNestedInput>;
  delete?: Maybe<Boolean>;
  disconnect?: Maybe<Boolean>;
  connect?: Maybe<AddressWhereUniqueInput>;
}

export interface NodeNode {
  id: ID_Output;
}

export interface NameSubscriptionPayload {
  mutation: MutationType;
  node: Name;
  updatedFields: String[];
  previousValues: NamePreviousValues;
}

export interface NameSubscriptionPayloadPromise
  extends Promise<NameSubscriptionPayload>,
    Fragmentable {
  mutation: () => Promise<MutationType>;
  node: <T = NamePromise>() => T;
  updatedFields: () => Promise<String[]>;
  previousValues: <T = NamePreviousValuesPromise>() => T;
}

export interface NameSubscriptionPayloadSubscription
  extends Promise<AsyncIterator<NameSubscriptionPayload>>,
    Fragmentable {
  mutation: () => Promise<AsyncIterator<MutationType>>;
  node: <T = NameSubscription>() => T;
  updatedFields: () => Promise<AsyncIterator<String[]>>;
  previousValues: <T = NamePreviousValuesSubscription>() => T;
}

export interface NameEdge {
  node: Name;
  cursor: String;
}

export interface NameEdgePromise extends Promise<NameEdge>, Fragmentable {
  node: <T = NamePromise>() => T;
  cursor: () => Promise<String>;
}

export interface NameEdgeSubscription
  extends Promise<AsyncIterator<NameEdge>>,
    Fragmentable {
  node: <T = NameSubscription>() => T;
  cursor: () => Promise<AsyncIterator<String>>;
}

export interface PersonPreviousValues {
  id: ID_Output;
  username: String;
  password: String;
  gender: String;
}

export interface PersonPreviousValuesPromise
  extends Promise<PersonPreviousValues>,
    Fragmentable {
  id: () => Promise<ID_Output>;
  username: () => Promise<String>;
  password: () => Promise<String>;
  gender: () => Promise<String>;
}

export interface PersonPreviousValuesSubscription
  extends Promise<AsyncIterator<PersonPreviousValues>>,
    Fragmentable {
  id: () => Promise<AsyncIterator<ID_Output>>;
  username: () => Promise<AsyncIterator<String>>;
  password: () => Promise<AsyncIterator<String>>;
  gender: () => Promise<AsyncIterator<String>>;
}

export interface NameConnection {
  pageInfo: PageInfo;
  edges: NameEdge[];
}

export interface NameConnectionPromise
  extends Promise<NameConnection>,
    Fragmentable {
  pageInfo: <T = PageInfoPromise>() => T;
  edges: <T = FragmentableArray<NameEdge>>() => T;
  aggregate: <T = AggregateNamePromise>() => T;
}

export interface NameConnectionSubscription
  extends Promise<AsyncIterator<NameConnection>>,
    Fragmentable {
  pageInfo: <T = PageInfoSubscription>() => T;
  edges: <T = Promise<AsyncIterator<NameEdgeSubscription>>>() => T;
  aggregate: <T = AggregateNameSubscription>() => T;
}

export interface AddressConnection {
  pageInfo: PageInfo;
  edges: AddressEdge[];
}

export interface AddressConnectionPromise
  extends Promise<AddressConnection>,
    Fragmentable {
  pageInfo: <T = PageInfoPromise>() => T;
  edges: <T = FragmentableArray<AddressEdge>>() => T;
  aggregate: <T = AggregateAddressPromise>() => T;
}

export interface AddressConnectionSubscription
  extends Promise<AsyncIterator<AddressConnection>>,
    Fragmentable {
  pageInfo: <T = PageInfoSubscription>() => T;
  edges: <T = Promise<AsyncIterator<AddressEdgeSubscription>>>() => T;
  aggregate: <T = AggregateAddressSubscription>() => T;
}

export interface Name {
  id: ID_Output;
  first: String;
  last: String;
}

export interface NamePromise extends Promise<Name>, Fragmentable {
  id: () => Promise<ID_Output>;
  first: () => Promise<String>;
  last: () => Promise<String>;
}

export interface NameSubscription
  extends Promise<AsyncIterator<Name>>,
    Fragmentable {
  id: () => Promise<AsyncIterator<ID_Output>>;
  first: () => Promise<AsyncIterator<String>>;
  last: () => Promise<AsyncIterator<String>>;
}

export interface NameNullablePromise
  extends Promise<Name | null>,
    Fragmentable {
  id: () => Promise<ID_Output>;
  first: () => Promise<String>;
  last: () => Promise<String>;
}

export interface PageInfo {
  hasNextPage: Boolean;
  hasPreviousPage: Boolean;
  startCursor?: String;
  endCursor?: String;
}

export interface PageInfoPromise extends Promise<PageInfo>, Fragmentable {
  hasNextPage: () => Promise<Boolean>;
  hasPreviousPage: () => Promise<Boolean>;
  startCursor: () => Promise<String>;
  endCursor: () => Promise<String>;
}

export interface PageInfoSubscription
  extends Promise<AsyncIterator<PageInfo>>,
    Fragmentable {
  hasNextPage: () => Promise<AsyncIterator<Boolean>>;
  hasPreviousPage: () => Promise<AsyncIterator<Boolean>>;
  startCursor: () => Promise<AsyncIterator<String>>;
  endCursor: () => Promise<AsyncIterator<String>>;
}

export interface AggregateAddress {
  count: Int;
}

export interface AggregateAddressPromise
  extends Promise<AggregateAddress>,
    Fragmentable {
  count: () => Promise<Int>;
}

export interface AggregateAddressSubscription
  extends Promise<AsyncIterator<AggregateAddress>>,
    Fragmentable {
  count: () => Promise<AsyncIterator<Int>>;
}

export interface AggregatePerson {
  count: Int;
}

export interface AggregatePersonPromise
  extends Promise<AggregatePerson>,
    Fragmentable {
  count: () => Promise<Int>;
}

export interface AggregatePersonSubscription
  extends Promise<AsyncIterator<AggregatePerson>>,
    Fragmentable {
  count: () => Promise<AsyncIterator<Int>>;
}

export interface AddressEdge {
  node: Address;
  cursor: String;
}

export interface AddressEdgePromise extends Promise<AddressEdge>, Fragmentable {
  node: <T = AddressPromise>() => T;
  cursor: () => Promise<String>;
}

export interface AddressEdgeSubscription
  extends Promise<AsyncIterator<AddressEdge>>,
    Fragmentable {
  node: <T = AddressSubscription>() => T;
  cursor: () => Promise<AsyncIterator<String>>;
}

export interface PersonConnection {
  pageInfo: PageInfo;
  edges: PersonEdge[];
}

export interface PersonConnectionPromise
  extends Promise<PersonConnection>,
    Fragmentable {
  pageInfo: <T = PageInfoPromise>() => T;
  edges: <T = FragmentableArray<PersonEdge>>() => T;
  aggregate: <T = AggregatePersonPromise>() => T;
}

export interface PersonConnectionSubscription
  extends Promise<AsyncIterator<PersonConnection>>,
    Fragmentable {
  pageInfo: <T = PageInfoSubscription>() => T;
  edges: <T = Promise<AsyncIterator<PersonEdgeSubscription>>>() => T;
  aggregate: <T = AggregatePersonSubscription>() => T;
}

export interface AggregateName {
  count: Int;
}

export interface AggregateNamePromise
  extends Promise<AggregateName>,
    Fragmentable {
  count: () => Promise<Int>;
}

export interface AggregateNameSubscription
  extends Promise<AsyncIterator<AggregateName>>,
    Fragmentable {
  count: () => Promise<AsyncIterator<Int>>;
}

export interface PersonSubscriptionPayload {
  mutation: MutationType;
  node: Person;
  updatedFields: String[];
  previousValues: PersonPreviousValues;
}

export interface PersonSubscriptionPayloadPromise
  extends Promise<PersonSubscriptionPayload>,
    Fragmentable {
  mutation: () => Promise<MutationType>;
  node: <T = PersonPromise>() => T;
  updatedFields: () => Promise<String[]>;
  previousValues: <T = PersonPreviousValuesPromise>() => T;
}

export interface PersonSubscriptionPayloadSubscription
  extends Promise<AsyncIterator<PersonSubscriptionPayload>>,
    Fragmentable {
  mutation: () => Promise<AsyncIterator<MutationType>>;
  node: <T = PersonSubscription>() => T;
  updatedFields: () => Promise<AsyncIterator<String[]>>;
  previousValues: <T = PersonPreviousValuesSubscription>() => T;
}

export interface AddressPreviousValues {
  id: ID_Output;
  street: String;
  city: String;
  state: String;
  zip: String;
}

export interface AddressPreviousValuesPromise
  extends Promise<AddressPreviousValues>,
    Fragmentable {
  id: () => Promise<ID_Output>;
  street: () => Promise<String>;
  city: () => Promise<String>;
  state: () => Promise<String>;
  zip: () => Promise<String>;
}

export interface AddressPreviousValuesSubscription
  extends Promise<AsyncIterator<AddressPreviousValues>>,
    Fragmentable {
  id: () => Promise<AsyncIterator<ID_Output>>;
  street: () => Promise<AsyncIterator<String>>;
  city: () => Promise<AsyncIterator<String>>;
  state: () => Promise<AsyncIterator<String>>;
  zip: () => Promise<AsyncIterator<String>>;
}

export interface AddressSubscriptionPayload {
  mutation: MutationType;
  node: Address;
  updatedFields: String[];
  previousValues: AddressPreviousValues;
}

export interface AddressSubscriptionPayloadPromise
  extends Promise<AddressSubscriptionPayload>,
    Fragmentable {
  mutation: () => Promise<MutationType>;
  node: <T = AddressPromise>() => T;
  updatedFields: () => Promise<String[]>;
  previousValues: <T = AddressPreviousValuesPromise>() => T;
}

export interface AddressSubscriptionPayloadSubscription
  extends Promise<AsyncIterator<AddressSubscriptionPayload>>,
    Fragmentable {
  mutation: () => Promise<AsyncIterator<MutationType>>;
  node: <T = AddressSubscription>() => T;
  updatedFields: () => Promise<AsyncIterator<String[]>>;
  previousValues: <T = AddressPreviousValuesSubscription>() => T;
}

export interface Address {
  id: ID_Output;
  street: String;
  city: String;
  state: String;
  zip: String;
}

export interface AddressPromise extends Promise<Address>, Fragmentable {
  id: () => Promise<ID_Output>;
  street: () => Promise<String>;
  city: () => Promise<String>;
  state: () => Promise<String>;
  zip: () => Promise<String>;
}

export interface AddressSubscription
  extends Promise<AsyncIterator<Address>>,
    Fragmentable {
  id: () => Promise<AsyncIterator<ID_Output>>;
  street: () => Promise<AsyncIterator<String>>;
  city: () => Promise<AsyncIterator<String>>;
  state: () => Promise<AsyncIterator<String>>;
  zip: () => Promise<AsyncIterator<String>>;
}

export interface AddressNullablePromise
  extends Promise<Address | null>,
    Fragmentable {
  id: () => Promise<ID_Output>;
  street: () => Promise<String>;
  city: () => Promise<String>;
  state: () => Promise<String>;
  zip: () => Promise<String>;
}

export interface BatchPayload {
  count: Long;
}

export interface BatchPayloadPromise
  extends Promise<BatchPayload>,
    Fragmentable {
  count: () => Promise<Long>;
}

export interface BatchPayloadSubscription
  extends Promise<AsyncIterator<BatchPayload>>,
    Fragmentable {
  count: () => Promise<AsyncIterator<Long>>;
}

export interface Person {
  id: ID_Output;
  username: String;
  password: String;
  gender: String;
}

export interface PersonPromise extends Promise<Person>, Fragmentable {
  id: () => Promise<ID_Output>;
  username: () => Promise<String>;
  password: () => Promise<String>;
  name: <T = NamePromise>() => T;
  gender: () => Promise<String>;
  address: <T = AddressPromise>() => T;
}

export interface PersonSubscription
  extends Promise<AsyncIterator<Person>>,
    Fragmentable {
  id: () => Promise<AsyncIterator<ID_Output>>;
  username: () => Promise<AsyncIterator<String>>;
  password: () => Promise<AsyncIterator<String>>;
  name: <T = NameSubscription>() => T;
  gender: () => Promise<AsyncIterator<String>>;
  address: <T = AddressSubscription>() => T;
}

export interface PersonNullablePromise
  extends Promise<Person | null>,
    Fragmentable {
  id: () => Promise<ID_Output>;
  username: () => Promise<String>;
  password: () => Promise<String>;
  name: <T = NamePromise>() => T;
  gender: () => Promise<String>;
  address: <T = AddressPromise>() => T;
}

export interface PersonEdge {
  node: Person;
  cursor: String;
}

export interface PersonEdgePromise extends Promise<PersonEdge>, Fragmentable {
  node: <T = PersonPromise>() => T;
  cursor: () => Promise<String>;
}

export interface PersonEdgeSubscription
  extends Promise<AsyncIterator<PersonEdge>>,
    Fragmentable {
  node: <T = PersonSubscription>() => T;
  cursor: () => Promise<AsyncIterator<String>>;
}

export interface NamePreviousValues {
  id: ID_Output;
  first: String;
  last: String;
}

export interface NamePreviousValuesPromise
  extends Promise<NamePreviousValues>,
    Fragmentable {
  id: () => Promise<ID_Output>;
  first: () => Promise<String>;
  last: () => Promise<String>;
}

export interface NamePreviousValuesSubscription
  extends Promise<AsyncIterator<NamePreviousValues>>,
    Fragmentable {
  id: () => Promise<AsyncIterator<ID_Output>>;
  first: () => Promise<AsyncIterator<String>>;
  last: () => Promise<AsyncIterator<String>>;
}

export type Long = string;

/*
The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1.
*/
export type Int = number;

/*
The `Boolean` scalar type represents `true` or `false`.
*/
export type Boolean = boolean;

/*
The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text.
*/
export type String = string;

/*
The `ID` scalar type represents a unique identifier, often used to refetch an object or as key for a cache. The ID type appears in a JSON response as a String; however, it is not intended to be human-readable. When expected as an input type, any string (such as `"4"`) or integer (such as `4`) input value will be accepted as an ID.
*/
export type ID_Input = string | number;
export type ID_Output = string;

/**
 * Model Metadata
 */

export const models: Model[] = [
  {
    name: "Name",
    embedded: false
  },
  {
    name: "Address",
    embedded: false
  },
  {
    name: "Person",
    embedded: false
  }
];

/**
 * Type Defs
 */

export const prisma: Prisma;

const express = require('express')
const { ApolloServer } = require('apollo-server-express')
const { prisma } = require('./generated/prisma-client')
const { typeDefs } = require('./schema.js')
const { resolvers } = require('./resolvers.js')

//const path = require('path')
const cors = require('cors')

const log = console.log.bind(console)

const app = express()
const port = 3005
const api = new ApolloServer({ 
  typeDefs, 
  resolvers, 
  formatError: (err) => {
      console.error(err)
      return err
  },
  context: () => ({ prisma }),
  playground: false
}) 

api.applyMiddleware({ app, path: '/api' })

app.use(cors())

/** @Deprecated No longer in use for backend to app, as assets serving is not a part of the app. */
// app.use(express.static(path.join(__dirname, '../front-end/build')))
app.use((_, res) => {
  res.status(404)
  res.type('txt').send('Not found')
})

app.get('/', (_, res) => res.send())

app.listen(port, () => log(`🚀 Server running at http://localhost:${port}${api.graphqlPath}`))

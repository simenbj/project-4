const { prisma } = require('./generated/prisma-client')
const fs = require('fs')


async function main() {
  const file = fs.readFileSync('./data.json')
  for (
    const {
      username, 
      password, 
      gender, 
      name: { first, last }, 
      address: { city, state, street, zip } 
    } of JSON.parse(file)
  ) {
      prisma.upsertPerson({
          where: { username },
          update: {
              username,
              password,
              gender,
              name: {
                update: {
                  first,
                  last
                }
              },
              address: {
                update: {
                  city,
                  state,
                  street,
                  zip
                }
              }
          },
          create: {
              username,
              password,
              gender,
              name: {
                create: {
                  first,
                  last
                }
              },
              address: {
                create: {
                  city,
                  state,
                  street,
                  zip
                }
              }
          }
      }).catch(err => console.error(err))
    }
}

main()

const findSortFunction = function({ orderBy = '' }) {
  switch(orderBy.toLowerCase()) {
        case 'ascending': {
            return 'username_ASC'
        }
        case 'descending': {
            return 'username_DESC'
        }    
        default: {
            return false
        }
    }
}

const findFilterFunction = function({ filter = '', type = 'username' }) {
  
      filter = filter.toLowerCase()

      switch(filter) {
        case 'starts_with': {
            return `${type}_starts_with`
        }
        case 'ends_with': {
            return `${type}_ends_with`
        }
        case 'contains': {
            return `${type}_contains`
        }
        case 'not_contains': {
            return `${type}_not_contains`
        }
        case 'not_starts_with': {
            return `${type}_not_starts_with`
        }
        case 'not_ends_with': {
            return `${type}_not_ends_with`
        }
        default: {
            return `${type}_starts_with`
        }
      }
}

const resolvers = { 
  Query: { 
    search: async (_, { search, limit = 500, orderBy, filter, type, cursor }, { prisma }) => {

      const query = { where: { [`${type}_starts_with`]: search }, first: limit }

      if (orderBy) {
          Object.assign(query, { orderBy: findSortFunction({ orderBy })})
      }

      if (filter) {
          query.where  = { [findFilterFunction({ filter, type })]: search }
      }

      const persons = await prisma.persons(cursor ? Object.assign(query, { after: cursor }) : query)
      
      return persons

    },
    count: async (_, { username }, { prisma }) => {
        const count = await prisma.personsConnection({where: { username_starts_with: username }}).aggregate().count()
        return count
    }
  },
  Mutation: {
    writePerson: async (_, { username, input }) => {
        const exists = await prisma.$exists.person({ username })
        if (exists) throw new Error(`Username ${username} is already taken`)

        const person = await prisma.createPerson({
          username,
          password: input.password,
          gender: input.gender,
          name: {
            create: {
              first: input.name.first,
              last: input.name.last
            }
          },
          address: {
            create: {
              city: input.address.city,
              state: input.address.state,
              street: input.address.street,
              zip: input.address.zip
            }
          }
        })

        return person
    },
    updatePerson: async (_, { username, input }, { prisma }) => {
        const { name, password, gender, address } = input
        const person = await prisma.upsertPerson({ 
          where: { username }, 
          update: {
            username,
            password,
            gender,
            name: {
              update: { 
                first: null,
                last: null,
                ...JSON.parse(JSON.stringify(name)) 
              }
            },
            address: {
              update: { 
                city: null,
                street: null,
                zip: null,
                state: null,
                ...JSON.parse(JSON.stringify(address)) 
              }
            }
          },
          create: {
            username,
            password,
            gender,
            name: {
              create: { 
                first: null,
                last: null,
                ...JSON.parse(JSON.stringify(name)) 
              }
            },
            address: {
              create: { 
                city: null,
                street: null,
                zip: null,
                state: null,
                ...JSON.parse(JSON.stringify(address)) 
              }
            }
          }
        })
        return person
    }
  },
  Person: {
      name(parent, _, { prisma }) {
          return prisma.person({id: parent.id}).name()
      },
      address(parent, _, { prisma }) {
          return prisma.person({id: parent.id}).address()
      }
  }
}

module.exports = { resolvers }

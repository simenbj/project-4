const { gql } = require('apollo-server-express')

const schema = gql`

    type Name {
        first: String!
        last: String!
    }

    type Address {
        street: String!
        city: String!
        state: String!
        zip: String!
    }

    type Person {
        id: ID
        username: String!
        password: String!
        name: Name!
        gender: String!
        address: Address
    }

    input nameInput {
        first: String!
        last: String!
    }

    input addressInput {
        street: String
        city: String
        state: String
        zip: String
    }

    input personInput {
        username: String!
        password: String!
        name: nameInput!
        gender: String!
        address: addressInput
    }

    type Query {
        search(search: String!, cursor: String, limit: Int, orderBy: String, filter: String, type: String!): [Person]
        count(username: String): Int
    }

    type Mutation {
        updatePerson(username: String!, input: personInput!): Person
        writePerson(username: String!, input: personInput!): Person
    }
`

module.exports = { typeDefs: schema }

# Prosjekt 4 - Brukerdatabase med React Native


# Om prosjektet - Hva er dette?

Dette er en React Native-applikasjon med tilhørende back-end basert på Node og GraphQL som tillater søking, filtrering, blaing og visuell fremstilling av dataene i en brukerdatabase. 


## Hvordan kjører jeg prosjektet?  \

1. git clone <repo-url fra gitlab>
2. Kjør så følgende kommandoer i terminal ifra src mappen:
    1. `npm install`


        _API-serveren skal nå kjøre på port 3005 på gruppe 34 sin side, så lenge du er på ntnu nett skal du ha tilgang._

2. `npm run start` starter Expo, Metro bundler på port 19003 og devtools i nettleser, åpne app på foretrukket måte.

Hvis alt har gått bra, bør appen være tilgjengelig via Expo med din foretrukne måte.


# Prosjektbeskrivelse


## Funksjonelle krav


### Applikasjon

Applikasjonen er bootstrappet med expo, og bruker expo til blant annet ikon håndtering og AsyncStorage. 

Implementasjonen består av en søkeboks, hvor en kan søke etter brukere i en database på brukernavn. Søk forekommer umiddelbart og listen blir oppdatert dynamisk fra resultat sett. Det er mulig filtrere søket med å trykke på boksen under søk med tittelen “Set search filter”. 

Søket debouncer, så søket skjer ikke umiddelbart. Dette er en naiv debouncing uten bibliotek som lodash eller rxjs, som kanskje ville kunne bruke Subjects eller annen implementasjon enn setTimeout. Søket går først mot cache med bruken Apollo grapqhl biblioteket for så å gå backend.

Resultatet rendres i en liste, hvor bruker blir presentert med kontroller for å bla i resultatsettet. Resultatsettet henter ikke alle data, men de markørene du spør etter. Apollo håndterer en persistent cache på device og lagrer dette med AsyncStorage. Det betyr at du kan ha data uten nettverkstilkobling etter første lasting av appen. Det er en melding som vises når du er offline, Appen må refreshes for å få en ny melding. Dette er nytt fra forrige øving, ellers gjenbrukes hele kodebiten både i redux og apollo.

Brukeren kan trykke på et listeelement for å åpne en detaljert visning. Her er tredje parts biblioteket “react-navigation” brukt, som anbefales for slikt i dokumentasjonen. Visningen har ingen spesielle handlinger. Denne komponenten er solid og har automatisk header, samt tilbake funksjonalitet, noe som gjorde denne til et naturlig valg. Edit er valgt fjernet i denne leveransen fra forrige, siden dette ikke stod som krav i oppgaven, og for å få mer react native trening er det skrevet en ny komponent. En kunne i stedet flyttet Form fra tidligere oppgave og gjenbrukt denne, siden all koden der eksisterer, men her ønsket jeg å lære litt mer. 

Filtrering og sortering er implementert gjennom å trykke på sorteringsrekkefølge under Sort by eller boksen for “Set search filter” som bruker. Alt håndteres i backend. En kan filtrere på alle valg som prisma støtter under brukernavn. 

AsyncStorage brukes også til å lagre søk en gjennomfører, dette skjer automatisk på debounce etter 2 sekunder eller ved å trykke utenfor å trigge “onBlur” på søkefeltet. Dette caches kun på klient siden uten bruk av apollo. Det er automatisk håndtering av JSON.parse, så JSON.stringify settes inn i cache når det lagres, det betyr at serialiseringen støtter alle JSON data typer i AsyncStorage. Det er en hardkodet nøkkel som lagrer ned en liste med maks 5 elementer, som operer som en stack.


### Back-end

Backend er samme som i prosjekt 3, med localhost etter kravene. Her er teknologiene:



*   Apollo Server 
*   Docker som kjører Prisma node app
*   Docker container med Postgres-database.
*   Prisma Api som en del av Apollo
*   Express.js
*   Cors modul for express ( unødvendig denne runden, men er ikke fjernet )

Koden er litt modifisert siden vi ikke server noen assets på lik linje, derfor operer en bare med api backend. Prisma er en fancy ORM som gjør mye mot graphql og forenkler arbeidet drastisk. Apollo håndterer selve request kallene og schema i front-end + backend. Bindingene til Prisma er derfor rent databaserte på typer som er et eget schema. 

For å få data i databasen er det viktig prisma kjører. Det finnes en data.json fil med data i og en populate.js som skriver data til den kjørende docker databasen via prisma containeren. 


### Gjenbruk av kode

Jeg har gjenbrukt store deler av kodebasen fra forrige prosjekt. Det begynte med å overføre css filer som var modulariserte til React native kompatibel css, erstatte html og dom eventer med native tags og tilpasse deretter. Slik er mesteparten av tiden gått og attributter og lignende er på samme linje blitt benyttet. Når jeg er alene krever det litt mer jobb og kjenne kodebasen er derfor et must, nå er det slik at jeg står bak store deler av både backend og front-end fra forrige prosjekt så det gikk fint. All js er mer eller mindre brukt om igjen, det er kun små refaktoreringer som har skjedd. Visningskode er skrevet på ny siden react native ikke støtter native html tags på lik linje. 

Egentlig er 70 - 80% av koden gjenbruk, spesielt innenfor logikk.


## Krav til bruk av teknologi

Git bruken var tenkt i starten, men når jeg er vært alene på prosjektet har ikke behovet vært der og finner heller ikke krav om god git bruk under krav for innleveringen. Git flow ble benyttet til å sette opp strukturen, og jeg har bundet inn linter og git hooks gjennom den velkjente modulen husky. Alt dette arver jeg fra oppsettet på forrige prosjekt. 

Jeg har brukt tredjeparts komponenter der det er nødvendig, for å håndtere tilstand, redux og mye mye mer. Underveis er flere komponenter brukt, men favoritten var react-navigation. Denne gjorde det ekstremt enkelt å multipage appen og minnet sterkt om react router i front-end. Wow. Utenom dette er apollo utvidet litt fra forrige innlevering med link og håndtering av feil er endelig gjort. Det gjelder også typo i søk. Utenom dette er react-native-modal-selector brukt til å velge filter for søk. Denne komponenten fungerer strålende. 

Redux er utvidet med et par actions, blant annet for håndtering av søk. Apollo er fikset med filtrering. 

Applikasjonen skal kjøre fint på android og på iphone da det er brukt SafeArea med android håndtering. Applikasjonen er e2e testet manuelt med å bruke simulator for alle hendelser i appen, dette ble gjort med en sjekk liste og underveis konstant i utviklingen. 

Irrelevant med tasking når en er alene, har derfor skippet denne delen i git. Begynte med det, men så ikke poenget. Hvem skal jeg informere?


### Testing

Jeg sitter med mac og har derfor testet appen med nyeste iphone. Appen fungerer strålende. Jeg har derimot ikke testet på android da jeg ikke besitter en android telefon. Alle iphone modellene fungerer fint i landscape, som appen er betenkt skal være for. 
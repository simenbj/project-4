import React, { useState, useEffect } from 'react'

import { Provider } from './src/Store/index.js' // Main store for app
import store from './src/Store/Store.js'
import { ApolloProvider } from '@apollo/react-hooks'
import ApolloClient from 'apollo-boost'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { persistCache } from 'apollo-cache-persist'

import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { ActivityIndicator, AsyncStorage } from 'react-native'
import {
	StyleSheet, Platform, SafeAreaView
} from 'react-native'
import App from './src/App.js'
import { DetailView } from './src/Components/index.js'
import { setCircumstancesError } from './src/Store/Actions.js'

const AppNavigator = createStackNavigator({
	Home: App,
	DetailView: DetailView
}, { initialRouteName: 'Home' })

const AppContainer = createAppContainer(AppNavigator)

const { container } = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		paddingTop: Platform.OS === 'android' ? 30 : 0
	},
})

// Approved by Trond Aalberg based on blackboard post about remote API
const port = 3005

export default function() {
	const [ client, setClient ] = useState(null)
	const [ error, setError ] = useState(false)

	useEffect(() => {
		const $ = async function() {
			const cache = new InMemoryCache()
			await persistCache({
				cache,
				storage: AsyncStorage,
			})

			setClient(
				new ApolloClient({
					uri: `http://it2810-34.idi.ntnu.no:${port}/api`,
					cache,
					onError: (({ networkError }) => {
						if (networkError && !error) {
							setError(true)
							store.dispatch(setCircumstancesError(true))
						}
					})
				})
			)
		}
		$()
	}, [])

	return (
		<SafeAreaView style={container}>
			{client ? (
				<ApolloProvider onError={() => null} client={client}>
					<Provider>
						<AppContainer />
					</Provider>
				</ApolloProvider>
			) :
				<ActivityIndicator />
			}
		</SafeAreaView>
	)
}


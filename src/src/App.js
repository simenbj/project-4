import React, { useEffect } from 'react'
import {
	Search,
	List
} from './Components'
import { ListPagination } from './Components/List/ListPagination'
import {
	StyleSheet, Text, View
} from 'react-native'
import { useSelector } from 'react-redux'

/** Provider component has global AppState, which is now available as a global state store
 * through use of redux. Use of redux will allow consumers to subscribe to changes or to
 * dispatch changes to other consumers through the AppState reducer, which has various
 * actions bound with different state tree modifications.
 *
 * Use of ReacDOM.render, and root instance node,
 * */


const { app, title, pagination } = StyleSheet.create({
	app: {
		flex: 1,
		width: '100%',
		paddingHorizontal: 10,
		paddingVertical: 20,
		height: '100%',
		position: 'relative'
	},
	title: {
		fontSize: 18,
		marginBottom: 10
	},
	pagination: {
		borderTopWidth: 1,
		borderTopColor: '#ccc',
		position: 'absolute',
		bottom: -10,
		right: 10,
		alignContent: 'center',
		alignItems: 'center',
		width: '100%',
		backgroundColor: 'white',
	}
})

export default function App({ navigation }) {
	const { selected = null } = useSelector(({ selected }) => ({ selected: selected['list-item'] }))
	const error = useSelector(({ circumstances: { error  } }) => error)

	useEffect(() => {
		if (selected) {
			navigation.navigate('DetailView')
		}
	}, [selected])

	return (
		<View style={app}>
			{error ? (
				<Text style={{ fontStyle: 'italic', color: 'tomato' }}>
                Network is Offline - data might be unavailable
				</Text>
			)
				: null}
			<Text style={title}>Search for usernames</Text>
			<Search autoCapitalize="none" />
			<List />
			<View style={pagination}>
				<ListPagination limit={10} pollInterval={2000} />
			</View>
		</View>
	)
}

App.navigationOptions = () => ({ headerTitle: () => <Text>Search application</Text> })

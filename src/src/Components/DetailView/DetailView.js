import React, { useMemo } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { useSelector } from 'react-redux'
import { capitalize } from '../../Utilities'

const { label, title, container, user } = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10
  },
  label: {
    fontWeight: 'bold',
    paddingTop: 10
  },
  title: {
    fontSize: 22,
    paddingVertical: 10,
    paddingHorizontal: 10
  },
  user: {
    paddingHorizontal: 10
  }
})

const flattenAndFilterSelected = function(obj, strs, rename, placeholder = 'No value') {
    const valuePairs = {}
    for (const key in obj) {
        if (Boolean(strs.filter(str => key.includes(str)).length)) continue
        if (obj[key] instanceof Object && !Array.isArray(obj[key]) && obj[key] !== null) {
            Object.assign(valuePairs, flattenAndFilterSelected(obj[key], strs, rename))
        } else {
            Object.assign(valuePairs, {[(key in rename ? rename[key] : key)]: obj[key] || placeholder})
        }
    }
    return valuePairs
}


const DetailView = function() {
    const { selected } = useSelector(({ selected }) => ({selected: selected['list-item'] }))
    const info = useMemo(() => flattenAndFilterSelected(selected, ['__', 'id'], {first: 'firstname', last: 'lastname'}), [selected])
    return (
        <View style={container}>
            <Text style={title}>User info</Text>
            {Object.entries(info).map(([key, value]) => {
                return (
                    <View {...{key}} style={user}>
                        <Text style={label}>{capitalize(key)}</Text>
                        <Text>{value}</Text>
                    </View>
                  )
            })}
        </View>
    )
}

export default DetailView

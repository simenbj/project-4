import React from 'react'
import {
	useDispatch, useSelector, batch
} from 'react-redux'
import { useEnter } from '../../../Hooks'
import {
	setView, setForm, setSelectedListItem
} from '../../../Store/Actions.js'
import { toggle } from './ChangeView.module.css'

const ChangeView = function() {
	const dispatch = useDispatch()
	const view = useSelector(({ view }) => view)
	const isEnter = useEnter()

	function toggleView({ target = document.activeElement } = {}) {
		if (target) {
			batch(() => {
				dispatch(setView(target.getAttribute('name')))
				dispatch(setForm(null))
				dispatch(setSelectedListItem(null))
			})
		}
	}

	function toggleViewWithKeys({ which, keyCode, ...e }) {
		if (!e) e = {}
		e && e.preventDefault && e.preventDefault()
		isEnter({ which, keyCode }) && toggleView()
	}
	return (
		<button
			onClick={toggleView}
			onTouchEnd={toggleView}
			tabIndex={0}
			onKeyDown={toggleViewWithKeys}
			className={toggle}
			name={view === 'wordcloud' ? 'list' : 'wordcloud'}>
			{view.toLowerCase() === 'wordcloud' ? 'Show list' : 'Show wordcloud'}
		</button>
	)
}

export default ChangeView

import React, { useEffect } from 'react'
import {
	ListItems,
	ListFilters
} from './index.js'
import { useDispatch, batch } from 'react-redux'
import {
	setSelectedListItem,
	setSelectedLimit,
} from '../../Store/Actions.js'
import {
	StyleSheet, View, ScrollView, Text
} from 'react-native'

const { list, listView, listContainer } = StyleSheet.create({
	list: {
		display: 'flex',
		flexDirection: 'column',
		width: '100%',
	},
	listView: {
		display: 'flex',
		flexDirection: 'column',
		height: '75%',
		width: '100%'
	},
	listContainer: {
		display: 'flex',
		flexDirection: 'column',
		width: '100%',
		height: '100%',
		marginBottom: 65
	}
})

/**
 * Main list is split into multiple parts, these list parts consists of various logical elementsi, props passed as functions, clever render functions, and abstractions, apollo and every requirment.
 *
  * */
const List = function({
	children = items => items,
	limit = 10
}) {
	const dispatch = useDispatch()
	useEffect(() => {
		batch(() => {
			dispatch(setSelectedLimit(limit))
			dispatch(setSelectedListItem(null))
		})
	}, [ limit, dispatch ])

	return (
		<View style={list}>
			<ListFilters>
				<Text style={{ fontSize: 12, fontStyle: 'italic' }}>Showing: {limit} items per page</Text>
			</ListFilters>
			<View style={listView}>
				<ScrollView vertical style={listContainer}>
					<ListItems {...{ children }} />
				</ScrollView>
			</View>
		</View>
	)
}
export default List

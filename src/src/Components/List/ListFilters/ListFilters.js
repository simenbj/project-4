import React, { useState } from 'react'
import {
	useDispatch, useSelector, batch
} from 'react-redux'
import {
	LIST_FILTER_ASCENDING,
	LIST_FILTER_DESCENDING
} from '../../../Utilities/Filters.js'
import { Range, capitalize } from '../../../Utilities'
import {
	setSelectedListFilter,
	setSelectedCursor,
	setSelectedListItem,
	setSelectedPage,
	setForm
} from '../../../Store/Actions.js'
import {
	TouchableWithoutFeedback, View, StyleSheet, Text, ScrollView
} from 'react-native'

const { container, title, inner, touchable, top, topEnd } = StyleSheet.create({
	container: {
		paddingBottom: 10,
		alignItems: 'flex-start'
	},
	title: {
		paddingVertical: 5,
		fontWeight: 'bold'
	},
	inner: {
		display: 'flex',
		flexDirection: 'row',
		textAlign: 'left',
		padding: 0
	},
	touchable: {
		paddingTop: 10,
		paddingRight: 10
	},
	top: {
		flexDirection: 'row',
		width: '100%',
		alignItems: 'center'
	},
	topEnd: { marginLeft: 'auto', }
})

const ListFilters = function({ children }) {
	const dispatch = useDispatch()
	const listFilter = useSelector(({ selected }) => selected['list-filter'])
	const [ selected, setSelected ] = useState(null)

	function filter(value) {
		if (selected !== value) {
			setSelected(value)
			batch(() => {
				dispatch(setSelectedListFilter(value))
				dispatch(setSelectedCursor([]))
				dispatch(setSelectedListItem(null))
				dispatch(setSelectedPage(1))
				dispatch(setForm(null))
			})
		}
	}

	return (
		<View style={container}>
			<View style={top}>
				<Text style={title}>Sort by</Text>
				<View style={topEnd}>{children}</View>
			</View>
			<ScrollView horizontal style={inner}>
				{Range.create(
					LIST_FILTER_ASCENDING,
					LIST_FILTER_DESCENDING,
					null
				).map(item => (
					<TouchableWithoutFeedback
						style={touchable}
						key={item || 'unsorted'}
						onPress={() => filter(item ? item.toLowerCase() : item)}
					>
						<Text style={item === listFilter ?  [ touchable, { color: 'blue' }] : touchable}>{capitalize((item || 'Unsorted'))}</Text>
					</TouchableWithoutFeedback>
				))}
			</ScrollView>
		</View>
	)
}

export default ListFilters

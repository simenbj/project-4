import React, {
	useEffect, useMemo, useCallback
} from 'react'
import {
	useDispatch, useSelector, batch
} from 'react-redux'
import { useQuery } from '@apollo/react-hooks'
import { SEARCH } from '../../../Queries'
import {
	setSelectedListItem,
	setCircumstancesError,
	setCircumstancesLoading,
	setList,
	setForm,
	setSelectedPage
} from '../../../Store/Actions.js'
import {
	Text, StyleSheet, FlatList, TouchableOpacity
} from 'react-native'

const { listItem } = StyleSheet.create({
	listItem: {
		paddingHorizontal: 10,
		paddingVertical: 20,
		backgroundColor: 'white',
		shadowColor: 'black',
		shadowOffset: { width: 0, height: 1 },
		shadowOpacity: 0.15
	}
})

const ListItems = function({ children = item => item }) {
	const dispatch = useDispatch()
	const search = useSelector(({ search }) => search)
	const hasError = useSelector(({ circumstances: { error } }) => error)
	const list = useSelector(({ list }) => list)
	const cursor = useSelector(({ selected: { cursor } }) =>
		cursor.length ? cursor[cursor.length - 1] : null
	)
	const orderBy = useSelector(({ selected }) => selected['list-filter'])
	const limit = useSelector(({ selected: { limit } }) => limit)
	const filter = useSelector(({ filter }) => filter)
	const page = useSelector(({ selected: { page } }) => page)
	const variables = useMemo(
		() =>
			orderBy
				? { search, limit, orderBy, type: 'username', filter }
				: { search, limit, type: 'username', filter },
		[ orderBy, search, limit, filter ]
	) // type could be dynamic, but is not implemented as not a requirement.

	const onError = useCallback(() => console.log('Error'))
	const {
		loading,
		error,
		data: { search: result = [] } = {},
		fetchMore,
	} = useQuery(SEARCH, {
		variables,
		onComplete: () => {
			dispatch(setSelectedPage(1))
		},
		onError,
		notifyOnNetworkStatusChange: true
	})

	const items = useMemo(() => children(list), [ list, children ])

	function setActiveItem(item) {
		batch(() => {
			dispatch(setSelectedListItem(item))
			dispatch(setForm(item))
		})
	}

	useEffect(() => {
		dispatch(setCircumstancesError(error))
	}, [ error, dispatch ])

	useEffect(() => {
		dispatch(setCircumstancesLoading(loading))
	}, [ loading, dispatch ])

	useEffect(() => {
		const fetchData = async function() {
			if (hasError) { return }
			try {
				await fetchMore({
					query: SEARCH,
					variables: cursor ? { ...variables, cursor } : variables,
					updateQuery: ({ entry } = {}, { fetchMoreResult: { search = [] } }) => {
						if (!entry) {
							return { search: [...search] }
						}
						if (!cursor) {
							return { search: entry.search ? [ ...entry.search, ...search ] : [...search] }
						}
						return {
							cursor,
							search: entry.search ? [ ...entry.search, ...search ] : [...search]
						}
					}
				})
			} catch (err) {
				dispatch(setCircumstancesError(true))
			}
		}
		fetchData()
	}, [ cursor, search, fetchMore, page, variables ])

	useEffect(() => {
		if (result && !error) {
			batch(() => {
				dispatch(setList(result))
				dispatch(setSelectedListItem(null))
			})
		}
	}, [ dispatch, result, error ])

	return (
		<FlatList
			data={items}
			keyExtractor={item => item.username}
			renderItem={({ item }) => (
				<TouchableOpacity
					style={listItem}
					onPress={() => setActiveItem(item)}
				>
					<Text>{typeof children(item) === 'string' || item.username || ''}</Text>
				</TouchableOpacity>
			)}
		/>
	)
}

export default ListItems

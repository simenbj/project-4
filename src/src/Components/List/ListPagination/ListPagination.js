import React, { useEffect, useMemo } from 'react'
import { FontAwesome } from '@expo/vector-icons'
import {
	useDispatch, useSelector, batch
} from 'react-redux'
import { useQuery } from '@apollo/react-hooks'
import {
	setSelectedPage,
	setSelectedCursor,
	setSelectedListItem,
	setSelectedListSelectedPosition,
	setForm
} from '../../../Store/Actions.js'
import { COUNT } from '../../../Queries'
import {
	View, StyleSheet, Text
} from 'react-native'
const { pager, button } = StyleSheet.create({
	pager: {
		display: 'flex',
		flexDirection: 'row',
		flexWrap: 'nowrap',
		paddingTop: 16,
		paddingHorizontal: 16,
		marginTop: 'auto',
		alignItems: 'baseline',
		justifyContent: 'center',
	},
	button: {
		margin: 10,
		padding: 0,
		backgroundColor: 'transparent',
		borderRadius: 10
	}
})

const ListPagination = function({ limit, pollInterval }) {
	const dispatch = useDispatch()
	const page = useSelector(({ selected: { page } }) => page)
	const list = useSelector(({ list }) => list)
	const search = useSelector(({ search }) => search)

	const { data: { count = 0 } = {} } = useQuery(COUNT, {
		pollInterval,
		variables: { username: search }
	})

	const pageCount = useMemo(() => (count ? Math.ceil(count / limit) : 1), [
		count,
		limit
	])

	function changePage(_page) {
		batch(() => {
			dispatch(setSelectedPage(_page))
			dispatch(
				setSelectedCursor(_page > page ? list[list.length - 1].id : null)
			)
			dispatch(setForm(null))
		})
	}

	useEffect(() => {
		batch(() => {
			dispatch(setSelectedListItem(null))
			dispatch(setSelectedListSelectedPosition(-1))
		})
	}, [ page, dispatch ])

	return pageCount > 1 ? (
		<View style={pager}>
			<FontAwesome.Button
				name="caret-left"
				style={button}
				backgroundColor="transparent"
				underlayColor="transparent"
				size={22}
				iconStyle={page > 1 ? ({ color: 'black' }) : ({ color: 'transparent' })}
				onPress={() => changePage(page - 1)}
				disabled={page <= 1}
			/>
			<View>
				<Text>{page} of {pageCount}</Text>
			</View>
			<FontAwesome.Button
				name="caret-right"
				style={button}
				backgroundColor="transparent"
				underlayColor="transparent"
				size={22}
				iconStyle={page === pageCount ? ({ color: 'transparent' }) : ({ color: 'black' })}
				onPress={() => changePage(page + 1)}
				disabled={page >= pageCount}
			/>
		</View>
	) : null
}

export default ListPagination

import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'
import { Button, StyleSheet } from 'react-native'

const SubmitButton = function({ isActive, isValid, style, onPress = () => console.warn('Button must implement a onPress handler') }) {
	const username = useSelector(({ form: { username } }) => username)
	const activeItem = useSelector(({ selected }) => selected['list-item'])
	const check = useMemo(() =>
		(isActive && activeItem && activeItem.username === username),
	    [ isActive, activeItem, username ]
	)

	function findLabel() {
		if (style) return 'Success'
		if (check) return 'Save'
		return 'Create'
	}
	return (
		<Button
			style={StyleSheet.create(style || ({
				userSelect: 'none',
				outline: 'none',
				background: check ? null : 'orange'
			}))}
			name={check ? 'save' : 'create'}
			disabled={!isValid || null}
			{...{ onPress }}
		>
			{findLabel()}
		</Button>
	)
}

export default SubmitButton

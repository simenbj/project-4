import React, { useState, useEffect } from 'react'
import {
	useDispatch, useSelector, batch
} from 'react-redux'
import { SearchFilter } from './SearchFilter'
import { SearchHistory } from './SearchHistory'
import {
	setSelectedPage,
	setSelectedCursor,
	setSelectedListItem,
	setSearch,
	setActiveSearch,
	setHistory
} from '../../Store/Actions.js'
import {
	View, TextInput, StyleSheet, AsyncStorage
} from 'react-native'

/** Search, a simple input component styled with a local scoped css file, with class search-
 * Uses state hook and dispatches to reducer through useDispatch
 *
 * */

const { search, searchBox } = StyleSheet.create({
	search: {
		display: 'flex',
		backgroundColor: 'white',
		width: '100%',
		shadowOffset: { width: 0, height: 2 },
		shadowColor: 'black',
		shadowOpacity: 0.2,
		paddingVertical: 15,
		paddingHorizontal: 10,
		height: 'auto'
	},
	searchBox: {
		width: '100%',
		marginBottom: 10
	}
})

let saveToHistoryDebounce = null
let debounce = null
const Search = function({ debounceTime = 200, ...props }) {
	const [ value, setSearchValue ] = useState('')
	const history = useSelector(({ history }) => history)
	const dispatch = useDispatch()
	const searchValue = useSelector(({ search }) => search)

	async function saveHistory(value) {
		try {
			if (
				!value ||
            !value.trim().length ||
            !history ||
            history.filter(item => item === value.trim()).length
			) {
				return
			}
			if (history.length < 5) {
				await AsyncStorage.setItem('@searchhistory', JSON.stringify([value.trim()].concat(history)))
				dispatch(setHistory([value.trim()].concat(history)))
			} else {
				await AsyncStorage.setItem(
					'@searchhistory',
					JSON.stringify([value.trim()].concat(history.slice(0, -1)))
				)
				dispatch(setHistory([value.trim()].concat(history.slice(0, -1))))
			}
			/** eslint-disable */
		} catch (err) {
			/** No need to handle error, just boundry for failing AsyncStorage */
		}
		/** eslint-enable */
	}

	function onChangeText(value) {
		setSearchValue(value)
		debounce && clearTimeout(debounce)
		saveToHistoryDebounce && clearTimeout(saveToHistoryDebounce)
		debounce = setTimeout(() => {
			batch(() => {
				dispatch(setSelectedPage(1))
				dispatch(setSelectedCursor([]))
				dispatch(setSelectedListItem(null))
				dispatch(setSearch(value))
			})
			saveToHistoryDebounce = setTimeout(() => saveHistory(value), 2000)
		}, debounceTime)
	}

	function placeholder() {
		const { children, placeholder } = props
		return children || placeholder || 'Search'
	}

	function onFocus() {
		dispatch(setActiveSearch(true))
	}

	function onBlur() {
		dispatch(setActiveSearch(false))
		saveToHistoryDebounce && clearTimeout(saveToHistoryDebounce)
		saveToHistoryDebounce = setTimeout(() => saveHistory(value), 1) // immediate invoke, but still clearable if event in stack
	}

	useEffect(() => {
		if (searchValue !== value) {
			setSearchValue(searchValue)
		}
	}, [searchValue])

	return (
		<View style={searchBox}>
			<TextInput
				{...props}
				placeholder={placeholder()}
				textContentType="none"
				name="search"
				style={search}
				{...{ value, onChangeText, onFocus, onBlur }}
			/>
			<SearchHistory />
			<SearchFilter />
		</View>
	)
}

export default Search

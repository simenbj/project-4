import React, { useState } from 'react'
import ModalSelector from 'react-native-modal-selector'
import { View, StyleSheet, Text } from 'react-native'
import { 
  STARTS_WITH, 
  ENDS_WITH, 
  CONTAINS, 
  NOT_CONTAINS, 
  NOT_ENDS_WITH, 
  NOT_STARTS_WITH 
} from '../../../Utilities/Filters.js'
import { useDispatch, batch } from 'react-redux'
import { 
  setSearchFilter,
  setSelectedPage,
  setSelectedCursor,
  setSelectedListItem
} from '../../../Store/Actions.js'
import { capitalize } from '../../../Utilities'


const { container, title } = StyleSheet.create({
    title: {
      paddingBottom: 2,
      fontWeight: 'bold'
    },
    container: {
        paddingVertical: 20
    }
})

const transformString = function(str) {
    return capitalize(str.replace(/\_/g, ' '))
}

const SearchFilter = function() {
    const [{ label, key }, setSelected] = useState({label: transformString(STARTS_WITH), key: STARTS_WITH})
    const dispatch = useDispatch()
    
    const items = [
      STARTS_WITH, 
      ENDS_WITH, 
      CONTAINS, 
      NOT_CONTAINS, 
      NOT_ENDS_WITH, 
      NOT_STARTS_WITH 
    ]
    
    function onChange(option) {
        setSelected(option)
        batch(() => {
            dispatch(setSearchFilter(option.key))
			dispatch(setSelectedPage(1))
			dispatch(setSelectedCursor([]))
			dispatch(setSelectedListItem(null))
        })
    }

  return (
      <View style={container}>
          <Text style={title}>Set search filter</Text>
          <ModalSelector
              data={items.map(item => ({key: item, label: transformString(item)}))}
              initValue={label}
              selectedKey={key}
              {...{onChange}}
          />
      </View>
    )
}

export default SearchFilter

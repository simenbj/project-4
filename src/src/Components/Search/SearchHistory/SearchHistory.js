import React, { useState, useEffect } from 'react'
import { AsyncStorage, ScrollView, Text, TouchableHighlight, StyleSheet } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { setSearch, setHistory } from '../../../Store/Actions.js'


const { container, text, highlight } = StyleSheet.create({
  container: {
    paddingVertical: 10,
    width: '100%'
  },
  text: {
    paddingHorizontal: 5,
    color: 'blue'
  },
  highlight: {
      marginTop: 10,
      borderRadius: 8,
      paddingVertical: 2,
      paddingHorizontal: 5
  }
})

const SearchHistory = function() {
    const dispatch = useDispatch()
    const [error, setError] = useState(false)
    const [loading, setLoading] = useState(true)
    const [items, setItems] = useState([])
    const history = useSelector(({ history }) => history)
  
    useEffect(() => {
        const fetchHistory = async function() {
            try {
                const result = await AsyncStorage.getItem('@searchhistory')
                if (result && Array.isArray(result)) {
                  dispatch(setHistory(result))
                  setError(false)
                  setItems(result)
                  setLoading(false)
                } else { throw 'No favorites found'}
            } catch(err) {
                setError('No favorites found') 
                setLoading(false)
            }
        }
        if (loading) fetchHistory()
    }, [loading])

    useEffect(() => {
        setItems(history)
        setError(false)
    }, [history])

    function onPress(storedValue) {
        storedValue && dispatch(setSearch(storedValue))
    }
    return (
      <ScrollView horizontal style={container}>
        {error || loading || items.length === 0 ? <Text>{loading ? 'Loading...' : error}</Text> : (
          items.map(storedValue => 
            <TouchableHighlight
                style={highlight}
                underlayColor="silver"
                key={storedValue}
                onPress={() => onPress(storedValue)}
            >
              <Text style={text}>{storedValue}</Text>  
            </TouchableHighlight>
          )
        )}
      </ScrollView>
    )
}

export default SearchHistory

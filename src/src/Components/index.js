/** Main component entry file and rexporter, buffer for making Components easier to handle in export */
import { Search } from './Search'
import { DetailView } from './DetailView'
import List from './List/List.js'
export {
	Search,
	List,
	DetailView
}

import { gql } from 'apollo-boost'

const Count = gql`
	query count($username: String) {
		count(username: $username)
	}
`

export default Count

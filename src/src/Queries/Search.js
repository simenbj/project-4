import { gql } from 'apollo-boost'

const Search = gql`
	query search(
		$search: String!
		$cursor: String
		$limit: Int
        $orderBy: String
        $filter: String
        $type: String!
	) {
		search(
		    search: $search
			cursor: $cursor
			limit: $limit
            orderBy: $orderBy
            filter: $filter
            type: $type
		) {
			id
			username
			password
			name {
				first
				last
			}
			gender
			address {
				street
				state
				city
				state
				zip
			}
		}
	}
`

export default Search

import { gql } from 'apollo-boost'

const UpsertUser = gql`
	mutation UpdatePerson($username: String!, $input: personInput!) {
		updatePerson(username: $username, input: $input) {
			id
			username
			password
			name {
				first
				last
			}
			gender
			address {
				street
				city
				state
				zip
			}
		}
	}
`

export default UpsertUser

//** Action Creators */

/** Import of Types */
import {
	SET_SEARCH,
	SET_LIST,
	SET_SELECTED_LIST_ITEM,
	SET_SELECTED_LIST_FILTER,
	SET_SELECTED_LIST_SELECTED_POSITION,
	SET_SELECTED_PAGE,
	SET_SELECTED_CURSOR,
	SET_SELECTED_LIMIT,
	SET_FORM,
	SET_USERNAME,
	SET_FIRST_NAME,
	SET_LAST_NAME,
	SET_GENDER,
	SET_ADDRESS_STREET,
	SET_ADDRESS_CITY,
	SET_ADDRESS_STATE,
	SET_ADDRESS_ZIP,
	SET_PASSWORD,
	SET_VIEW,
	SET_CIRCUMSTANCES_ERROR,
	SET_CIRCUMSTANCES_LOADING,
	SET_ACTIVE_FORM,
	SET_ACTIVE_SEARCH,
	SET_ACTIVE_MODAL,
	SET_SEARCH_FILTER,
	SET_FAVORITES,
	SET_HISTORY
} from './Types.js'

//** searchReducer Action Creators  */

export function setSearch( payload ) {
	return { type: SET_SEARCH, payload }
}

//** listReducer Action Creators  */

export function setList( payload ) {
	return { type: SET_LIST, payload }
}

//** selectedReducer Action Creators  */

export function setSelectedListItem( payload ) {
	return { type: SET_SELECTED_LIST_ITEM, payload }
}
export function setSelectedListFilter( payload ) {
	return { type: SET_SELECTED_LIST_FILTER, payload }
}
export function setSelectedListSelectedPosition( payload ) {
	return { type: SET_SELECTED_LIST_SELECTED_POSITION, payload }
}
export function setSelectedPage( payload ) {
	return { type: SET_SELECTED_PAGE, payload }
}
export function setSelectedCursor( payload ) {
	return { type: SET_SELECTED_CURSOR, payload }
}
export function setSelectedLimit( payload ) {
	return { type: SET_SELECTED_LIMIT, payload }
}

//** formReducer Action Creators  */

export function setForm( payload ) {
	return { type: SET_FORM, payload }
}
export function setUsername( payload ) {
	return { type: SET_USERNAME, payload }
}
export function setFirstName( payload ) {
	return { type: SET_FIRST_NAME, payload }
}
export function setLastName( payload ) {
	return { type: SET_LAST_NAME, payload }
}
export function setGender( payload ) {
	return { type: SET_GENDER, payload }
}
export function setAddressStreet( payload ) {
	return { type: SET_ADDRESS_STREET, payload }
}
export function setAddressCity( payload ) {
	return { type: SET_ADDRESS_CITY, payload }
}
export function setAddressState( payload ) {
	return { type: SET_ADDRESS_STATE, payload }
}
export function setAddressZip( payload ) {
	return { type: SET_ADDRESS_ZIP, payload }
}
export function setPassword( payload ) {
	return { type: SET_PASSWORD, payload }
}

//** viewReducer Action Creators  */

export function setView( payload ) {
	return { type: SET_VIEW, payload }
}

//** circumstancesReducer Action Creators  */

export function setCircumstancesError( payload ) {
	return { type: SET_CIRCUMSTANCES_ERROR, payload }
}
export function setCircumstancesLoading( payload ) {
	return { type: SET_CIRCUMSTANCES_LOADING, payload }
}

//** activeReducer Action Creators  */

export function setActiveForm( payload ) {
	return { type: SET_ACTIVE_FORM, payload }
}
export function setActiveSearch( payload ) {
	return { type: SET_ACTIVE_SEARCH, payload }
}
export function setActiveModal( payload ) {
	return { type: SET_ACTIVE_MODAL, payload }
}

export function setSearchFilter( payload ) {
	return { type: SET_SEARCH_FILTER, payload }
}

export function setFavorites( payload ) {
	return { type: SET_FAVORITES, payload }
}

export function setHistory( payload ) {
	return { type: SET_HISTORY, payload }
}

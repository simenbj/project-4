const formFields = [
	{
		field: 'username',
		name: 'username',
		placeholder: 'Username',
		query: true
	},
	{
		field: 'password',
		name: 'password',
		placeholder: 'Password'
	},
	{
		field: 'name.first',
		name: 'firstname',
		placeholder: 'Firstname' // because of the dataset, but others are ofc okay :) :)
	},
	{
		field: 'name.last',
		name: 'lastname',
		placeholder: 'Lastname' // because of the dataset, but others are ofc okay :) :)
	},
	{
		field: 'address.city',
		name: 'city',
		placeholder: 'City'
	},
	{
		field: 'address.street',
		name: 'street',
		placeholder: 'Street'
	},
	{
		field: 'address.state',
		name: 'state',
		placeholder: 'State'
	},
	{
		field: 'address.zip',
		name: 'zip',
		placeholder: 'Zip'
	},
	{
		field: 'gender',
		name: 'gender',
		placeholder: 'Male / Female' // because of the dataset, but others are ofc okay :) :)
	}
]

export default formFields

/** Appstate, rootReducer funtion and initial state to use with redux  */

/** Import of Types */
import {
	SET_SEARCH,
	SET_LIST,
	SET_SELECTED_LIST_ITEM,
	SET_SELECTED_LIST_FILTER,
	SET_SELECTED_LIST_SELECTED_POSITION,
	SET_SELECTED_PAGE,
	SET_SELECTED_CURSOR,
	SET_SELECTED_LIMIT,
	SET_FORM,
	SET_USERNAME,
	SET_FIRST_NAME,
	SET_LAST_NAME,
	SET_GENDER,
	SET_ADDRESS_STREET,
	SET_ADDRESS_CITY,
	SET_ADDRESS_STATE,
	SET_ADDRESS_ZIP,
	SET_PASSWORD,
	SET_CIRCUMSTANCES_ERROR,
	SET_CIRCUMSTANCES_LOADING,
	SET_ACTIVE_FORM,
	SET_ACTIVE_SEARCH,
	SET_SEARCH_FILTER,
	SET_FAVORITES,
	SET_HISTORY
} from './Types.js'

import { STARTS_WITH } from '../Utilities/Filters.js'

/** Since redux behaviour is of JSON, we can not supply functions, and it is generally considered not best practice to keep
 * formstate as a part of the redux store. We will for the sake of this task do so though, as it does not really matter for this use case.
 * This comment is written as awareness to this, and we think it is very much clearer to structure the code here
 * */

/** Initial state tree with populated data */

export const Form = function() {
	this.username = String()
	this.name = { first: String(), last: String() }
	this.gender = String()
	this.address = {
		street: String(),
		city: String(),
		state: String(),
		zip: String()
	}
	this.password = String()
}


export const initState = {
	search: '',
	filter: STARTS_WITH,
	favorites: [],
	history: [],
	list: [],
	selected: {
		'list-item': null,
		'list-filter': null,
		'list-selected-position': -1,
		page: 1,
		cursor: [],
		limit: 0
	},
	form: new Form(),
	circumstances: {
		error: null,
		loading: false,
	},
	active: {
		form: false,
		search: false,
		modal: false
	}
}

export const filterReducer = function(state = initState.filter, { payload, type }) {
	switch (type) {
		case SET_SEARCH_FILTER: {
			return payload
		}
		default: {
			return state
		}
	}
}

export const searchReducer = function(state = initState.search, { payload, type }) {
	switch (type) {
		case SET_SEARCH: {
			return payload
		}
		default: {
			return state
		}
	}
}

export const listReducer = function(state = initState.list, { payload, type }) {
	switch (type) {
		case SET_LIST: {
			return payload
		}
		default: {
			return state
		}
	}
}

export const selectedReducer = function(state = initState.selected, { payload, type }) {
	switch (type) {
		case SET_SELECTED_LIST_ITEM: {
			const clone = payload ? JSON.parse(JSON.stringify(payload)) : null
			if (clone === null) {
				return { ...state, 'list-item': null }
			}
			return {
				...state,
				'list-item': clone,
			}
		}
		case SET_SELECTED_LIST_FILTER: {
			return {
				...state,
				'list-filter': payload
			}
		}
		case SET_SELECTED_LIST_SELECTED_POSITION: {
			return {
				...state,
				'list-selected-position': payload
			}
		}
		case SET_SELECTED_PAGE: {
			return {
				...state,
				page: payload
			}
		}
		case SET_SELECTED_CURSOR: {
			if (!payload) {
				return {
					...state,
					cursor: state.cursor.slice(0, state.cursor.length - 1)
				}
			}
			if (payload && Array.isArray(payload) && payload.length === 0) {
				return {
					...state,
					cursor: []
				}
			}
			return {
				...state,
				cursor: state.cursor.concat([payload])
			}
		}
		case SET_SELECTED_LIMIT: {
			return {
				...state,
				limit: payload
			}
		}
		default: {
			return state
		}
	}
}

export const formReducer = function(state = initState.form, { payload, type }) {
	switch (type) {
		case SET_FORM: {
			return payload || new Form()
		}
		case SET_USERNAME: {
			return { ...state, username: payload }
		}
		case SET_FIRST_NAME: {
			return {
				...state,
				name: {
					...state.name,
					first: payload
				}
			}
		}
		case SET_LAST_NAME: {
			return {
				...state,
				name: {
					...state.name,
					last: payload
				}
			}
		}
		case SET_GENDER: {
			return { ...state, gender: payload }
		}
		case SET_ADDRESS_STREET: {
			return {
				...state,
				address: {
					...state.address,
					street: payload
				}
			}
		}
		case SET_ADDRESS_CITY: {
			return {
				...state,
				address: {
					...state.address,
					city: payload
				}
			}
		}
		case SET_ADDRESS_STATE: {
			return {
				...state,
				address: {
					...state.address,
					state: payload
				}
			}
		}
		case SET_ADDRESS_ZIP: {
			return {
				...state,
				address: {
					...state.address,
					zip: payload
				}
			}
		}
		case SET_PASSWORD: {
			return { ...state, password: payload }
		}
		default: {
			return state
		}
	}
}

export const circumstancesReducer = function(state = initState.circumstances, { payload, type }) {
	switch (type) {
		case SET_CIRCUMSTANCES_ERROR: {
			return {
				...state,
				error: payload
			}
		}
		case SET_CIRCUMSTANCES_LOADING: {
			return {
				...state,
				loading: payload
			}
		}
		default: {
			return state
		}
	}
}

export const activeReducer = function(state = initState.active, { payload, type }) {
	switch (type) {
		case SET_ACTIVE_FORM: {
			return {
				...state,
				form: payload
			}
		}
		case SET_ACTIVE_SEARCH: {
			return {
				...state,
				search: payload
			}
		}
		default: {
			return state
		}
	}
}

export const favoritesReducer = function(state = initState.favorites, { payload, type }) {
	switch (type) {
		case SET_FAVORITES: {
			return (payload || [])
		}
		default: {
			return state
		}
	}
}

export const historyReducer = function(state = initState.history, { payload, type }) {
	switch (type) {
		case SET_HISTORY: {
			return (payload || [])
		}
		default: {
			return state
		}
	}
}


import { createStore, combineReducers } from 'redux'
import {
	searchReducer,
	listReducer,
	selectedReducer,
	formReducer,
	circumstancesReducer,
	activeReducer,
	filterReducer,
	favoritesReducer,
	historyReducer
} from './Reducers.js'

export default createStore(
	combineReducers({
		search: searchReducer,
		filter: filterReducer,
		list: listReducer,
		favorites: favoritesReducer,
		history: historyReducer,
		selected: selectedReducer,
		form: formReducer,
		circumstances: circumstancesReducer,
		active: activeReducer
	})
)

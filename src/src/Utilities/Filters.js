/** List Sorting */
export const LIST_FILTER_ASCENDING  = 'ascending'
export const LIST_FILTER_DESCENDING = 'descending'

/** Search Filters */

export const STARTS_WITH        = 'starts_with'
export const ENDS_WITH          = 'ends_with'
export const CONTAINS           = 'contains'
export const NOT_CONTAINS       = 'not_contains'
export const NOT_STARTS_WITH    = 'not_starts_with'
export const NOT_ENDS_WITH      = 'not_ends_with'


/** Types */
export const USERNAME = 'username'
export const GENDER = 'gender'

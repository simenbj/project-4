/** Range helper, used to make use of number iteration in Components render methods, exposed
 * methods are minimally implemented. Based of Functional programming ideas.
 * */

export default class Range {
	constructor(start, end) {
		this.start = end ? start : 0
		this.end = end || start
	}

	[Symbol.iterator]() {
		let i = this.start
		return {
			next: () => ({
				value: i++,
				done: i > this.end
			})
		}
	}

	static create(...args) {
		return [...args]
	}

	static map(self, callback) {
		if (callback instanceof Function && Array.isArray(self)) {
			const list = []
			let index = 0
			for (const i of new Range(self.length)) {
				list[index++] = callback(self[i], i, self)
			}
			return list
		} else if (!(callback instanceof Function)) {
			throw new Error(`arguments[1] must be a function, was ${callback}`)
		} else if (!Array.isArray(self)) {
			throw new Error(`arguments[0] must be an Array, was ${self}`)
		}
		return []
	}

	static of(self, callback) {
		if (callback instanceof Function && typeof self === 'number') {
			const list = []
			let index = 0
			for (const i of new Range(self)) {
				list[index++] = callback(i + 1, self)
			}
			return list
		} else if (!(callback instanceof Function)) {
			throw new Error(`arguments[1] must be a function, was ${callback}`)
		} else if (!(typeof self === 'number')) {
			throw new Error(`arguments[0] must be a Number, was ${self}`)
		}
		return []
	}

	static TakeUntil(self, start, end, callback) {
		if (
			typeof start === 'number' &&
			typeof end === 'number' &&
			Array.isArray(self) &&
			callback instanceof Function
		) {
			const list = []
			let index = 0
			for (const i of new Range(start, end > self.length ? self.length : end)) {
				list[index++] = callback(self[i], i, self)
			}
			return list
		} else if (!Array.isArray(self)) {
			throw new Error(`arguments[0] must be an Array, was ${self}`)
		} else if (!(typeof start === 'number')) {
			throw new Error(`arguments[1] must be a number, was ${start}`)
		} else if (!(typeof end === 'number')) {
			throw new Error(`arguments[2] must be a number, was ${end}`)
		} else if (!(callback instanceof Function)) {
			throw new Error(`arguments[3] must be a function, was ${callback}`)
		}
		return []
	}
}
